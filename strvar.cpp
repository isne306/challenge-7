﻿//strvar.cpp

#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <cstring>
#include <string>
#include "strvar.h"

using namespace std;

namespace strvarken
{
	//Uses cstddef and cstdlib
	StringVar::StringVar(int size) : max_length(size)
	{
		value = new char[max_length + 1];
		value[0] = '\0';
	}

	//Uses cstddef and cstdlib
	StringVar::StringVar() : max_length(100)
	{
		value = new char[max_length + 1];
		value[0] = '\0';
	}

	// Uses cstring, cstddef and cstdlib
	StringVar::StringVar(const char a[]) : max_length(strlen(a))
	{
		value = new char[max_length + 1];
		for (int i=0;i<max_length;i++)
		{
			value[i] = a[i];
		}
		value[max_length] = '\0';
		//strcpy(value, a);
	}

	//Uses cstring, cstddef and cstdlib
	StringVar::StringVar(const StringVar& string_object) : max_length(string_object.length())
	{
		value = new char[max_length + 1];
		for (int i=0;i<max_length;i++)
		{
			value[i] = string_object.value[i];
		}
		value[max_length] = '\0';
		//strcpy(value, string_object.value);
	}

	StringVar::~StringVar()
	{
		delete [] value;
	}

	//Uses cstring
	int StringVar::length() const
	{
		return strlen(value);
	}

	//Uses iostream
	void StringVar::input_line(istream& ins)
	{
		ins.getline(value, max_length + 1);
	}

	//Uses iostream
	ostream& operator << (ostream& outs, const StringVar& the_string)
	{
		outs << the_string.value;
		return outs;
	}

	// 1.
	string StringVar::copy_piece(int x,int y)
	{
		string Str;

		for(int i=x-1; i<x+y-1; i++)
		{
			Str = Str + value[i];
		}

		return Str;
	}
	// 2.	
	char StringVar::one_char(int x)
	{
		return value[x-1];
	}
	// 3.
	void StringVar::set_char(int x, char y)
	{
		value[x-1] = y ;
	}
	// 4.
	bool operator == (StringVar a,StringVar b)
	{
		int s=0;
		for(int i=0;i<a.length();i++)
		{
			if(a.value[i] == b.value[i])
			{
				s++;
			}
		}
		if(a.length()==s)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	// 5.
	StringVar operator + (StringVar a,StringVar b)
	{
		StringVar c;
		int j=0;
		for(int i=0;i<a.length();i++)
		{
			c.value[j]+=a.value[i];
			j++;
		}
		for(int i=0;i<b.length();i++)
		{
			c.value[j]+=b.value[i];
			j++;
		}
		return c;
	}
	// 6.
	istream& operator >>(istream& ins, const StringVar& the_string)
	{
		ins >> the_string.value;
		return ins;
	}
}//strvarken