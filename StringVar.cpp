﻿//StringVar.cpp

#include <iostream>
#include "strvar.h"

void conversation(int max_name_size);
//Continues converstation with user

int main()
{
	using namespace std;
	conversation(30);
	cout << "End of Demo.\n";
	return 0;
}

//Demo function
void conversation(int max_name_size)
{
	using namespace std;
	using namespace strvarken;

	StringVar your_name(max_name_size), our_name("Borg");

	cout << "What is your name?\n";
	your_name.input_line(cin);
	cout << "We are " << our_name << endl;
	cout << "We will meet again " << your_name << endl;

	// 1.copy_piece func.
	cout << your_name.copy_piece(3,4);
	cout << endl;

	// 2.one_char func.
	cout << your_name.one_char(2);
	cout << endl;

	// 3.set_char func.
	your_name.set_char(4,'b');

	// print full string
	cout << your_name << endl;

	StringVar newStr;
	cout << "\nInput new sting: ";
	newStr.input_line(cin);
	cout << newStr << endl;

	// 4.overloaded operator ==
	if(your_name == newStr)
	{
		cout << "SAME SAME" << endl;
	}
	else
	{
		cout << "NOT SAME" << endl;
	}

	// 5.overloaded operator +
	StringVar newStr2 = your_name + newStr;
	cout << newStr2 << endl;


	// 6.overloaded operator >>
	StringVar newStr3;
	cout << "\nInput: ";
	cin >> newStr3;
	cout << newStr3 << endl;
}